function  testing (note_names, target_freq, start_note, end_note, dist1, dist2, URL, notesCount, net)

    %     Testing accuracy ------

         extension = '.wav';

    corr_count = 0;

         nn_output =[];
         nn_out_note= [];
         for loop= start_note:end_note


             name = char(note_names(loop));
             file = strcat(URL, name, extension)
             
             
             if ~exist(file, 'file')
                msg = strcat('Note ', {' '} , name, {' '}, 'does note exist.',{' '},'Make sure you have selected correct folder.');
                errordlg(msg,'Error');
                return
             end

            [y,Fs] = audioread(file);
            % sound(y,Fs);

            [m,n] = size(y);   %if stereo merge channels
            if (n > 1)
            y = sum(y, 2) / size(y, 2);
            end

                    yy = y;

                    % FFT analysis -------------------------
                    L=length(yy);
                    time = (1/Fs)*(1:L);
                    df=Fs/L;
                    freq=-Fs/2:df:Fs/2-df;
                    ampl=abs(fftshift(fft(yy))/length(fft(yy)));
                    freq= freq(length(freq)/2:length(freq));
                    ampl= ampl(length(ampl)/2:length(ampl));

                    ampl = (ampl-0)/(max(ampl)-0); % using normalisation to equalise amplitudes


        %           FINDING PEAK data ------------------------------

                    in=[];
                    inf=[];
                    freq_count = 0;
                    for i = 1:notesCount


                        fpnts = find(freq> target_freq(i).*dist1 & freq < target_freq(i).*dist2 );

                        freq_count = freq_count + length(fpnts);
                        spnts = sum(ampl(fpnts));
                        in = [in , spnts];



                    end

        %       Getting inputs and targets for NN ------

                in_n = (in-0)/((freq_count*1)-0); % normalisation formula: (x-min)/(max-min)

        %          NN --------

                s = net(in_n');
                nn_output = [nn_output, s];
                ni = find(s == max(s));
                nn_out_note = [nn_out_note, ni];
                if( ni + start_note-1 == loop )
                    corr_count = corr_count + 1;
                end

         end

         
    % drawing answer matrix

        figure('units','normalized','outerposition',[0 0 1 1])
        h = heatmap(note_names(start_note:end_note), note_names(start_note:end_note) ,nn_output');
        h.Title = 'Percentages of recognised notes';
        h.XLabel = 'Recognised as';
        h.YLabel = 'Notes';
        h.FontSize = 8; 
        h.Colormap = gray;


    %     calculating percentage
    avr = mean(max(nn_output));
    perc =  100 - ((1 - avr) * 100)
    stpr = sprintf('%.4f', perc)
    corr_perc = (corr_count / (end_note-start_note+1)) *100
    message = strcat('Accuracy: ', stpr , '% ', {' Correct: '}, int2str(corr_perc), {'% '});
    helpdlg(message,'Result')

end