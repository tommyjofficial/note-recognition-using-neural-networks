function  analyse (note_names, target_freq, start_note, end_note, dist1, dist2, URL, notesCount, noprt)

    %   ANALYSING SAMPLES ---------------------
     input = [];
     target=[];
     
     
%      makes array of file names
     notes = [];
     files = dir(fullfile(URL, '*.wav'));
     for i = 1: length(files)
         
         name{i} = strrep(files(i).name,'.wav','');
     end
     
     
     for i = 1 : length(name)
         bl = 1;
         names = strsplit(name{i},'_');
         for j = 1:length(names)
             [ia, ~] = ismember(note_names, names{j});
             no = find(ia, 1, 'first');
             if(no > end_note || no < start_note)
                bl = 0;
                break
             end
         end 
         if (bl == 1)
            notes{end+1} = name{i};
         end
     end
     
     if isempty(notes)
            msg = strcat('Samples do not exist in selected range.',{' '},'Make sure you have selected correct range and folder.');
            errordlg(msg,'Error');
           return
        end
     
        

        for loop=1:length(notes)
 
%          creating the path string
         extension = '.wav';
         note_char = char(notes(loop));
         file = strcat(URL, note_char, extension);
         
%  checks if file exists
        if ~exist(file, 'file')
            msg = strcat('Note ', {' '} , note_char, {' '}, 'does note exist.',{' '},'Make sure you have selected correct range and folder.');
            errordlg(msg,'Error');
           return
        end
        
%         target_notes created to set targets for chords
        target_notes =[];
        names = strsplit(notes{loop},'_');
        for j = 1:length(names)
             [ia, ~] = ismember(note_names, names{j});
             no = find(ia, 1, 'first');
             target_notes{end+1} = no;
        end 
        
         

        [y,Fs] = audioread(file);

        [m,n] = size(y);   %if stereo merge channels
        if (n > 1)
        y = sum(y, 2) / size(y, 2);
        end



    %     SPLITING RECORDING into parts --------------------------

        prt = length(y) ./ noprt;
        l = length(y);
        pt =  floor(l  / prt);
        re = rem(l ,prt);
        p1 = 1;
        p2 = prt; 
        
        for i = 1: pt

                yy = y(p1:p2);

                % FFT analysis -------------------------
                L=length(yy);
                df=Fs/L;
                freq=-Fs/2:df:Fs/2-df;
                ampl=abs(fftshift(fft(yy))/length(fft(yy)));
                freq= freq(length(freq)/2:length(freq));
                ampl= ampl(length(ampl)/2:length(ampl));

                ampl = (ampl-0)/(max(ampl)-0); % using normalisation to equalise amplitudes


    %             FINDING PEAK data ------------------------------
                in=[];
                inf=[];

                freq_count = 0;
                for i = 1:notesCount


                    fpnts = find(freq> target_freq(i).*dist1 & freq < target_freq(i).*dist2 );

                    freq_count = freq_count + length(fpnts);
                    spnts = sum(ampl(fpnts));
                    in = [in , spnts];


                end

    %       Getting inputs and targets for NN ------

            in_n = (in-0)/((freq_count*1)-0); % normalisation formula: (x-min)/(max-min)

            input  = [input; in_n];

            t(1:end_note-start_note+1)= 0;
                      
            for k = 1: length(target_notes)
                t(target_notes{k}-start_note+1) = 1;
            end
            
            
            
            target = [target;t];

    %       updating boundaries for audio split
            p1 = p1 + prt;
            p2 = p2  + prt;


            end


        

        end


     
        if exist('input.mat', 'file') == 2 &&  exist('target.mat', 'file') == 2
            
        new_input = input ;
        load('input.mat');
            if(~isequal(input, new_input))
                
                input = [input; new_input];
                save('input.mat','input');

                new_target = target;
                load('target.mat');
                target = [target; new_target];
                save('target.mat','target');
                
            
            else
                save('input.mat','input');
                save('target.mat','target'); 
            end
        else
          save('input.mat','input');
          save('target.mat','target'); 
        end

end