
function varargout = GUI(varargin)
% Begin initialization code -
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @GUI_OpeningFcn, ...
                   'gui_OutputFcn',  @GUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code 


% --- Executes just before GUI is made visible.
function GUI_OpeningFcn(hObject, eventdata, handles, varargin)
handles.output = hObject; % handles used to store variables
handles.note_names = ["C0", "C#0", "D0", "D#0", "E0", "F0", "F#0", "G0", "G#0", "A0", "A#0", "B0", ... 
              "C1", "C#1", "D1", "D#1", "E1", "F1", "F#1", "G1", "G#1", "A1", "A#1", "B1", ...
              "C2", "C#2", "D2", "D#2", "E2", "F2", "F#2", "G2", "G#2", "A2", "A#2", "B2", ...
              "C3", "C#3", "D3", "D#3", "E3", "F3", "F#3", "G3", "G#3", "A3", "A#3", "B3", ...
              "C4", "C#4", "D4", "D#4", "E4", "F4", "F#4", "G4", "G#4", "A4", "A#4", "B4", ...
              "C5", "C#5", "D5", "D#5", "E5", "F5", "F#5", "G5", "G#5", "A5", "A#5", "B5", ...
              "C6", "C#6", "D6", "D#6", "E6", "F6", "F#6", "G6", "G#6", "A6", "A#6", "B6", ...
              "C7", "C#7", "D7", "D#7", "E7", "F7", "F#7", "G7", "G#7", "A7", "A#7", "B7", ...
              "C8", "C#8", "D8", "D#8", "E8", "F8", "F#8", "G8", "G#8", "A8", "A#8", "B8", ];        
handles.notesCount =  108;  %count of all notes of all octaves
% Update handles structure
guidata(hObject, handles);

% deletes dataset from previous session
delete input.mat
delete target.mat

 % makes some buttons disabled 
set(handles.nntrain,'Enable','off') ;
set(handles.test_btn,'Enable','off') ;
set(handles.recognition,'Enable','off') ;


% --- Outputs from this function are returned to the command line.
function varargout = GUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function tunning_Callback(hObject, eventdata, handles)
% hObject    handle to tunning (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA
handles.tunn = get(hObject,'String');
guidata(hObject, handles);


% Hints: get(hObject,'String') returns contents of tunning as text
%        str2double(get(hObject,'String')) returns contents of tunning as a double


% --- Executes during object creation, after setting all properties.
function tunning_CreateFcn(hObject, eventdata, handles)
% hObject    handle to tunning (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in lowestnote.
function lowestnote_Callback(hObject, eventdata, handles)
% hObject    handle to lowestnote (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
 handles.s_nt = get(hObject,'Value');
 guidata(hObject, handles);

% Hints: contents = cellstr(get(hObject,'String')) returns lowestnote contents as cell array
%        contents{get(hObject,'Value')} returns selected item from lowestnote


% --- Executes during object creation, after setting all properties.
function lowestnote_CreateFcn(hObject, eventdata, handles)
% hObject    handle to lowestnote (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in highestnote.
function highestnote_Callback(hObject, eventdata, handles)
% hObject    handle to highestnote (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.e_nt = get(hObject,'Value');
guidata(hObject, handles);
% Hints: contents = cellstr(get(hObject,'String')) returns highestnote contents as cell array
%        contents{get(hObject,'Value')} returns selected item from highestnote


% --- Executes during object creation, after setting all properties.
function highestnote_CreateFcn(hObject, eventdata, handles)
% hObject    handle to highestnote (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function nosamples_Callback(hObject, eventdata, handles)
% hObject    handle to nosamples (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.no_smpl = str2double(get(hObject,'String'));
guidata(hObject, handles);

% Hints: get(hObject,'String') returns contents of nosamples as text
%        str2double(get(hObject,'String')) returns contents of nosamples as a double


% --- Executes during object creation, after setting all properties.
function nosamples_CreateFcn(hObject, eventdata, handles)
% hObject    handle to nosamples (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in samplefolder.
function samplefolder_Callback(hObject, eventdata, handles)
% hObject    handle to samplefolder (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
selpath = uigetdir;
if  (selpath ~= 0)
handles.URL_s =  strcat (selpath, '\');
guidata(hObject, handles);
end


% --- Executes on button press in analysesamples.
function analysesamples_Callback(hObject, eventdata, handles)
% hObject    handle to analysesamples (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% handles.output = hObject;

if ~isfield(handles, 'tunn')
    handles.tunn = 440;
    guidata(hObject, handles);
end

if ~isfield(handles, 's_nt')
    errordlg('Please select lowest note','Error');
elseif ~isfield(handles, 'e_nt')
    errordlg('Please select highest note','Error');
elseif handles.s_nt >= handles.e_nt
    errordlg('Wrong range selected','Error');
    
else
    [target_freq, start_note, end_note, dist1, dist2 ] = setting (handles.tunn, handles.s_nt, handles.e_nt, handles.notesCount);
    handles.target_freq = target_freq;
    handles.start_note =start_note;
    handles.end_note = end_note;
    handles.dist1 =dist1;
    handles.dist2 = dist2;
    guidata(hObject, handles);


    if ~isfield(handles, 'no_smpl')
        handles.no_smpl = 30;
        guidata(hObject, handles);
    end
    if ~isfield(handles, 'URL_s')
        errordlg('Please select folder with samples','Error');
    else
    helpdlg('Please wait, it may take a couple of minutes, you will be notified when it is finished.',' ');
    analysis(handles.note_names, handles.target_freq, handles.start_note, handles.end_note, handles.dist1, handles.dist2, handles.URL_s, handles.notesCount, handles.no_smpl);
    helpdlg('Analysed!',' ');
    set(handles.nntrain,'Enable','on') ;
    end

end


% --- Executes on button press in nntrain.
function nntrain_Callback(hObject, eventdata, handles)
% hObject    handle to nntrain (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if ~exist ('input.mat', 'file')
    errordlg('Samples have not been analysed yet!','Error');
else
[net] = training();
handles.net = net;
guidata(hObject, handles);
set(handles.test_btn,'Enable','on') ;
set(handles.recognition,'Enable','on') ;
end


% --- Executes on button press in choosefldtesting.
function choosefldtesting_Callback(hObject, eventdata, handles)
% hObject    handle to choosefldtesting (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
selpath = uigetdir;
if  (selpath ~= 0)
handles.URL_t =  strcat (selpath, '\');
guidata(hObject, handles);
end

% --- Executes on button press in test_btn.
function test_btn_Callback(hObject, eventdata, handles)
% hObject    handle to test_btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if ~isfield(handles, 'URL_t')
        errordlg('Please select folder with testing samples','Error');
else
    set(handles.nntrain,'Enable','off') ;
    set(handles.recognition,'Enable','off') ;
    set(handles.analysesamples,'Enable','off') ;
    helpdlg('This may take some time, please wait.',' ');
testing (handles.note_names, handles.target_freq, handles.start_note, handles.end_note, handles.dist1, handles.dist2, handles.URL_t, handles.notesCount, handles.net)
    set(handles.nntrain,'Enable','on') ;
    set(handles.recognition,'Enable','on') ;
    set(handles.analysesamples,'Enable','on') ;
end


% --- Executes on button press in recordingbtn.
function recordingbtn_Callback(hObject, eventdata, handles)
% hObject    handle to recordingbtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[FileName,PathName,FilterIndex] = uigetfile( {'*.wav'},  'Pick a file');
if  (FilterIndex ~= 0)
handles.f =  strcat (PathName, FileName);
guidata(hObject, handles);
end

% --- Executes on button press in recognition.
function recognition_Callback(hObject, eventdata, handles)
% hObject    handle to recognition (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if ~isfield(handles, 'f')
        errordlg('Please select a recording','Error');
else
    set(handles.nntrain,'Enable','off') ;
    set(handles.test_btn,'Enable','off') ;
    set(handles.analysesamples,'Enable','off') ;
    helpdlg('This may take some time, please wait.',' ');
recognition(handles.note_names, handles.target_freq, handles.start_note, handles.end_note, handles.dist1, handles.dist2, handles.notesCount, handles.net, handles.f);
    set(handles.nntrain,'Enable','on') ;
    set(handles.test_btn,'Enable','on') ;
    set(handles.analysesamples,'Enable','on') ;
end

