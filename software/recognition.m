function  recognition (note_names, target_freq, start_note, end_note, dist1, dist2, notesCount, net, f)

    %     NOTE RECOGNITION PART ===========================================

         file = f;
         
            if ~exist(file, 'file')
                msg = strcat('Recording ', {' '} , file, {' '}, 'does note exist.',{' '},'Make sure you have selected correct folder.');
                errordlg(msg,'Error');
                return
             end

            [y,Fs] = audioread(file);
            % sound(y,Fs);

            [m,n] = size(y);   %if stereo merge channels
            if (n > 1)
            y = sum(y, 2) / size(y, 2);
            end


    %  DEVIDING RECORDING INTO PARTS BY NOTES ------------------------------

        time = (1/Fs)*(1:length(y));

        windowSize = 5700;
        b = (1/windowSize)*ones(1,windowSize);
        a = 1;

        [up,lo] = envelope(y, 2000,'rms');
        p = filter(b,a,up); 
        p1 = filter(b,a,p);
        

        ny = p;


    [pks,locs] = findpeaks(p1, 'MinPeakHeight', 0.01);


    %finding the borders between notes
    int = [];
    pk = [];
    for i = 2: length(locs)

        no = find ( ny == min ( ny( locs(i-1) : locs(i) ) ) );  
        int = [int; no];
        mm = min ( ny( locs(i-1) : locs(i) ) );
        pk = [pk; mm ];
    end


    % split into parts by borders
    p1 = 1;
    parts = [];
    maxis = [];
    for i = 1 : length(int)
        part = y(p1:int(i));
        parts{end+1} =part;

        maxi = up(p1:int(i));
        maxis{end+1} =maxi;

        p1 = int(i) + 1;
    end
    parts{end+1} = y(int(end):length(y));
    maxis{end+1} = up(int(end):length(up));


    % find biggest peaks in splits
    y_p = abs(y);
    borders = [];

    for i = 1 : length(maxis)  
        no = find ( y_p == max( abs(parts{i}) ) );
        borders = [borders; no];


    end

    pk0 = zeros(1, length(pk));
    pk01 = zeros(1, length(pks));


    % spliting into parts again by new borders
    p1 = 1;
    parts = [];
    int(end+1) = length(y);
    for i = 1 : length(int)
        part = y(borders(i):int(i));
        parts{end+1} =part;


    end






         %     NOTE RECOGNITION  ------------------

            nn_output =[];
            nn_out_note= [];

            for i = 1: length(parts)

                    yy = parts{i};

                    % FFT analysis -------------------------
                    L=length(yy);
                    df=Fs/L;
                    freq=-Fs/2:df:Fs/2-df;
                    ampl=abs(fftshift(fft(yy))/length(fft(yy)));
                    freq= freq(length(freq)/2:length(freq));
                    ampl= ampl(length(ampl)/2:length(ampl));

                    ampl = (ampl-0)/(max(ampl)-0); % using normalisation to equalise amplitudes


        %           FINDING PEAK data ------------------------------

                    in=[];
                    inf=[];
                    freq_count = 0;
                    for i = 1:notesCount


                        fpnts = find(freq> target_freq(i).*dist1 & freq < target_freq(i).*dist2 );

                        freq_count = freq_count + length(fpnts);
                        spnts = sum(ampl(fpnts));
                        in = [in , spnts];

                    end

        %       Getting inputs and targets for NN ------

                in_n = (in-0)/((freq_count*1)-0); % normalisation formula: (x-min)/(max-min)

        %          NN --------

                s = net(in_n');
                nn_output = [nn_output, s];
                ni = find(s == max(s));
                nn_out_note = [nn_out_note, ni];

            end


        result_notes = note_names(nn_out_note + start_note-1);

    %     drawing answer graph
        for i = 1: length(result_notes)
            txt {i} = char(result_notes(i));
        end

        av = mean (abs(y));
        mh = max(y);
        mnh = min(y);

        bor = borders./ Fs;

        for i = 1 : length(bor)  
           pl(i) = bor(i)+2000/Fs; 
        end
        hh(1:length(pl)) = mh*1.1;

    % drawing answer matrix
    for i = 1 : length(nn_output(1,:))
        n_name{i} = strcat({'Note '}, {int2str(i)});
    end
        figure('units','normalized','outerposition',[0 0 1 1])
        h = heatmap(n_name, note_names(start_note:end_note) ,nn_output);
        h.Title = 'Percentages of recognised notes';
        h.FontSize = 8; 
        h.Colormap = gray;
        
        
        % drawing answer graph
        figure ('Units', 'Normalized', 'OuterPosition', [0.15, 0.15, 0.7, 0.7])       
        hold on
        plot (time, y)
        line([bor bor], [mh*1.3 mnh], 'Color','red')
        text(pl, hh, txt,'FontSize',11);
        xlabel('Time (s)')
        ylabel('Amplitude')
        ylim([mnh*0.8 mh*1.5])
        xlim([bor(1)*0.8 bor(end)*1.05])
        hold off
        
        % message with accuracy percentage
        avr = mean(max(nn_output));
        perc =  100 - ((1 - avr) * 100)
        message = strcat('Accuracy: ',int2str(perc), '% ');
%         helpdlg(message,'Result')

end