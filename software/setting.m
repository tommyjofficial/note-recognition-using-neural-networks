function [t_freq, start_note, end_note, dist1, dist2 ] = setting (tunn, st_n, end_n, notesCount)

    %     SETTINGS ----------------------------
    a4_no = 58;
    a4=tunn;   % tuning frequency
    log_no = nthroot(2, 12);
    dist1 = 1 - (log_no-1) *0.20;
    dist2 = 1 + (log_no-1) *0.20;

    % peaks_number = 8;
    start_note = st_n;
    end_note = end_n;

    %  CALCULATION of FREQIENCIES BY TUNING (A440 standard for example) ----------
    nf = a4;
    for i = a4_no+1:notesCount

        t_freq(i)= nf * log_no;
        nf = nf * log_no;

    end
    nf = a4;
    for i = a4_no-1:-1:1

        t_freq(i)= nf / log_no;
        nf = nf / log_no;
    end
    t_freq(a4_no) = a4;

end