function [net] =  training ()

%  NEURAL NETWORK TRAINING ------------------------
 
    load('input.mat', 'input');
    load('target.mat', 'target');
    x = input';
    t = target';

    
    trainFcn = 'trainscg';  
    % Create a Network
    hiddenLayerSize = 20;
    net = patternnet(hiddenLayerSize);     
    net = configure(net,x,t);                
    
    net.trainFcn= trainFcn;
    net.trainParam.epochs=2000;  % epochs number
    
    % Setup Division of Data for Training, Validation, Testing
    net.divideParam.trainRatio = 70/100;
    net.divideParam.valRatio = 15/100;
    net.divideParam.testRatio = 15/100;
    
    % Train the Network
    [net,tr] = train(net,x,t);

end