# Note Recognition Using Neural Networks - Thesis Project

## ABSTRACT

This project provides an approach to note recognition from audio files based on Neural Networks, when Neural Networks are trained and note recognition is performed using audio recordings, all of which were made using the same instrument. This allows note recognition to be performed more accurately using one instrumentsí unique qualities as advantages rather than disadvantages for the Neural Network match. The testing shows high accuracy of up to 99% for single notes, of this method. The chosen approach of analysing and preparing data from audio recordings to be used by Neural Networks, has proved that this method, however cannot identify chords with the accuracy as high as of single notes. Due to the way audio recordings are processed this approach can only be applied for certain types of instruments, such as pianos, guitars and basses.

### Using instructions: documentation.doc

